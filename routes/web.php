<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/', [
	'as'	=>'landing',
	'uses'	=>'FrontController@landing'
]);

Route::get('/about', [
	'as'	=>'about',
	'uses'	=>'FrontController@about'
]);

Route::get('/akta', [
	'as'	=>'akta',
	'uses'	=>'FrontController@akta'
]);

Route::get('/legalitas', [
	'as'	=>'legalitas',
	'uses'	=>'FrontController@legalitas'
]);

Route::get('/fabrikasi', [
	'as'	=>'fabrikasi',
	'uses'	=>'FrontController@fabrikasi'
]);

Route::get('/machining', [
	'as'	=>'machining',
	'uses'	=>'FrontController@machining'
]);

Route::get('/general-trading', [
	'as'	=>'trading',
	'uses'	=>'FrontController@trading'
]);

Route::get('/contact-us', [
	'as'	=>'contact',
	'uses'	=>'FrontController@contact'
]);

//============================================================

Route::get('/admin', [
	'as'	=>'create',
	'uses'	=>'BackController@create'
]);

Route::post('/store', [
	'as'	=>'store',
	'uses'	=>'BackController@store'
]);

Route::get('/edit/{id}', [
	'as'	=>'edit',
	'uses'	=>'BackController@edit'
]);

Route::put('/update/{id}', [
	'as'	=>'update',
	'uses'	=>'BackController@update'
]);

