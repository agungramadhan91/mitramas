<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Mitramas Muda Mandiri</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport">
	<meta content="" name="keywords">
	<meta content="" name="description">

	<!-- Facebook Opengraph integration: https://developers.facebook.com/docs/sharing/opengraph -->
	<meta property="og:title" content="">
	<meta property="og:image" content="">
	<meta property="og:url" content="">
	<meta property="og:site_name" content="">
	<meta property="og:description" content="">

	<!-- Twitter Cards integration: https://dev.twitter.com/cards/  -->
	<meta name="twitter:card" content="summary">
	<meta name="twitter:site" content="">
	<meta name="twitter:title" content="">
	<meta name="twitter:description" content="">
	<meta name="twitter:image" content="">

	<!-- Fav and touch icons -->
	<link rel="shortcut icon" href="{{ asset('img/icons/favicon.png') }}">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('img/icons/114x114.png') }}">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('img/icons/72x72.png') }}">
	<link rel="apple-touch-icon-precomposed" href="{{ asset('img/icons/default.png') }}">

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900" rel="stylesheet">

	<!-- Bootstrap CSS File -->
	<link href="{{ asset('lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

	<!-- Libraries CSS Files -->
	<link href="{{ asset('lib/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
	<link href="{{ asset('lib/owlcarousel/owl.carousel.min.css') }}" rel="stylesheet">
	<link href="{{ asset('lib/owlcarousel/owl.theme.min.css') }}" rel="stylesheet">
	<link href="{{ asset('lib/owlcarousel/owl.transitions.min.css') }}" rel="stylesheet">

	<!-- Lightbox prettyPhoto -->
	<link href="{{ asset('css/prettyPhoto.css') }}" rel="stylesheet">
	<link href="{{ asset('css/display_image.css') }}" rel="stylesheet">
	

	<!-- Main Stylesheet File -->
	<link href="{{ asset('css/style.css') }}" rel="stylesheet">

	<!--Your custom colour override - predefined colours are: colour-blue.css, colour-green.css, colour-lavander.css, orange is default-->
	<link href="#" id="colour-scheme" rel="stylesheet">

	<!-- =======================================================
		Theme Name: Flexor
		Theme URL: https://bootstrapmade.com/flexor-free-multipurpose-bootstrap-template/
		Author: BootstrapMade.com
		Author URL: https://bootstrapmade.com
	======================================================= -->
</head>

<body class="{{ Request::path() == '/' ? 'page-index has-hero':'page-about>' }}">
	
	@include('layouts.header-content')

	<!-- ======== @Region: #content ======== -->
	<div id="content">

	@if(Request::path() == 'akta' || Request::path() == 'about' || Request::path() == 'legalitas')
		<div class="container" id="about">
	      	<div class="row">
	        	<!--main content-->
		        <div class="col-md-9 col-md-push-3">
			        @yield('content')
		        </div>
		        
		        <!-- sidebar -->
		        <div class="col-md-3 col-md-pull-9 sidebar visible-md-block visible-lg-block">
					<ul class="nav nav-pills nav-stacked">
		        		
						@yield('links')
						
					</ul>
		        </div>
	      	</div>
	    </div>
	@else
		@yield('content')
	@endif
		
  	</div>
  <!-- /content -->
  

  <!-- ======== @Region: #footer ======== -->
  @include('layouts.footer')

  <!-- Required JavaScript Libraries -->
  <script src="{{ asset('lib/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('lib/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('lib/owlcarousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('lib/stellar/stellar.min.js') }}"></script>
  <script src="{{ asset('lib/waypoints/waypoints.min.js') }}"></script>
  <script src="{{ asset('lib/counterup/counterup.min.js') }}"></script>
  <script src="{{ asset('contactform/contactform.js') }}"></script>

	@yield('footer')

  <!-- Template Specisifc Custom Javascript File -->
  <script src="{{ asset('js/custom.js') }}"></script>

  <!--Custom scripts demo background & colour switcher - OPTIONAL -->
  <script src="{{ asset('js/color-switcher.js') }}"></script>

  <script src="{{ asset('js/jquery.prettyPhoto.js') }}"></script>

  <!--Contactform script -->
  <!-- <script src="contactform/contactform.js') }}"></script> -->

</body>

</html>
