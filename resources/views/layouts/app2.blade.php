<!DOCTYPE HTML>

 <html>
    <head>
    	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta charset="utf-8">
        <!-- Description, Keywords and Author -->
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Mitramas Muda Mandiri</title>

		<link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/fonts.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
	</head>

    <body>
    	<header role="header">
        	<div class="container">
                <nav role="header-nav" class="navy">
                    <ul>
                        <li class="{{ Request::path() == '/' ? 'nav-active':'' }}">
                            <a href="{{ route('landing') }}" title="Home">Home</a>
                        </li>

                        @if(!Auth::check())
                            @if(Request::path() == 'akta')
                                <li>
                                    <a href="{{ route('legalitas') }}" title="Legalitas">Home - Legalitas</a>
                                </li>
                            @elseif(Request::path() == 'legalitas')
                                <li>
                                    <a href="{{ route('akta') }}" title="Akta">Home - Akta</a>
                                </li>
                            @endif
                        @endif

                        <li class="{{ Request::path() == 'fabrikasi' ? 'nav-active':'' }}">
                            <a href="{{ route('fabrikasi') }}" title="Fabrikasi">Fabrikasi</a>
                        </li>
                        <li class="{{ Request::path() == 'machining' ? 'nav-active':'' }}">
                            <a href="{{ route('machining') }}" title="Machining">Machining</a>
                        </li>
                        <li class="{{ Request::path() == 'general-trading' ? 'nav-active':'' }}">
                            <a href="{{ route('trading') }}" title="Trading">General Trading</a>
                        </li>

                        @if(Auth::check())
                            <li class="{{ Request::path() == 'admin-page' ? 'nav-active':'' }}">
                                <a href="{{ route('create') }}" title="Admin">Admin Page</a>
                            </li>
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        @else
                            <li class="{{ Request::path() == 'contact-us' ? 'nav-active':'' }}">
                                <a href="{{ route('contact') }}" title="Contact">Hubungi Kami</a>
                            </li>
                        @endif
                    </ul>
                </nav>
            </div>
        </header>

        <main role="main-inner-wrapper" class="container">
            
            @yield('content')

        </main>

        <footer role="footer">
            <nav role="footer-nav">
            	<ul>
                	<li><a href="{{ route('landing') }}" title="Home">Home</a></li>
                    <li><a href="{{ route('fabrikasi') }}" title="Fabrikasi">Fabrikasi</a></li>
                    <li><a href="{{ route('machining') }}" title="Machining">Machining</a></li>
                    <li><a href="{{ route('trading') }}" title="Trading">General Trading</a></li>
                    <li><a href="{{ route('contact') }}" title="Contact">Hubungi Kami</a></li>
                </ul>
            </nav>

            <!-- <ul role="social-icons">
            	<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-flickr" aria-hidden="true"></i></a></li>
            </ul> -->

            <p class="copy-right">&copy; 2017 | Aeres Hamada | All rights Reserved</p>
        </footer>

        <script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>
		<script src="{{ asset('js/nav.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/custom.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>

        @yield('script')

        <script src="{{ asset('js/html5shiv.js') }}" type="text/javascript"></script>
    </body>
</html>