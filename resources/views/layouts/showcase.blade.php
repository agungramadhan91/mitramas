<!--Showcase-->
<div class="showcase block block-border-bottom-grey">
	<div class="container">
		<h2 class="block-title">
			Showcase Fabrikasi, Machining, dan General Trading
		</h2>
		<p>
			This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.
		</p>

		<div class="item-carousel" data-toggle="owlcarousel" data-owlcarousel-settings='{"items":3, "pagination":false, "navigation":true, "itemsScaleUp":true}' >

			@if(count($businesses) > 5)
				@foreach($businesses->random(5) as $business)
					<div class="item">
						<a href="#" class="overlay-wrapper">
							{{--  {!! Html::image(asset('image/'.$business->photo), null, ['class'=>'img-responsive underlay']) !!}  --}}
							<div class="wrapper-image" style="background-image: url({{ asset('image/'.$business->photo) }});"></div>
							<span class="overlay">
								<span class="overlay-content"> <span class="h4">{{ ucfirst($business->description) }}</span> </span>
							</span>
						</a>
						<div class="item-details bg-noise">
							<h4 class="item-title">
								@if($business->type == 'trading')
									<a href="{{ route($business->type) }}">General {{ ucfirst($business->type) }}</a>
								@else
									<a href="{{ route($business->type) }}">{{ ucfirst($business->type) }}</a>
								@endif
							</h4>
							{{--  <a href="{{ $business->id }}" class="btn btn-more"><i class="fa fa-plus"></i>baca kelanjutannya</a>  --}}
						</div>
					</div>
				@endforeach
			@endif
		  	
		  	<div class="item">
				<a href="{{ asset('img/showcase/project1.png') }}" data-rel="prettyPhoto" class="overlay-wrapper">
					<img src="{{ asset('img/showcase/project1.png') }}" alt="Project 1 image" class="img-responsive underlay">
					<span class="overlay">
					  	<span class="overlay-content"> <span class="h4">Project 1</span> </span>
					</span>
					{{--  <div class="active item">
						<a href="images/temp/post-img-1.jpg" data-rel="prettyPhoto" title="Disney’s Brave">
							<img src="{{ asset('img/showcase/project1.png') }}" alt="" />
						</a>
						<div class="carousel-desc gradient"><strong>Brave</strong><span>&laquo;Change your fate.&raquo;</span></div>
					</div>  --}}
				</a>
				<div class="item-details bg-noise">
				  	<h4 class="item-title">
					  	<a href="#">Project 1</a>
					</h4>
				  	<a href="#" class="btn btn-more"><i class="fa fa-plus"></i>Read more</a>
				</div>
		  	</div>

		  	<div class="item">
				<a href="#" class="overlay-wrapper">
					<img src="{{ asset('img/showcase/project2.png') }}" alt="Project 2 image" class="img-responsive underlay">
					<span class="overlay">
					  	<span class="overlay-content"> <span class="h4">Project 2</span> </span>
					</span>
				</a>
				<div class="item-details bg-noise">
				  	<h4 class="item-title">
					  <a href="#">Project 2</a>
					</h4>
				  	<a href="#" class="btn btn-more"><i class="fa fa-plus"></i>Read more</a>
				</div>
		  	</div>

		  	<div class="item">
				<a href="#" class="overlay-wrapper">
					<img src="{{ asset('img/showcase/project3.png') }}" alt="Project 3 image" class="img-responsive underlay">
					<span class="overlay">
					  	<span class="overlay-content"> <span class="h4">Project 3</span> </span>
					</span>
			  	</a>
				<div class="item-details bg-noise">
			  		<h4 class="item-title">
				  		<a href="#">Project 3</a>
					</h4>
			  		<a href="#" class="btn btn-more"><i class="fa fa-plus"></i>Read more</a>
				</div>
		  	</div>

			<div class="item">
				<a href="#" class="overlay-wrapper">
					<img src="{{ asset('img/showcase/project4.png') }}" alt="Project 4 image" class="img-responsive underlay">
					<span class="overlay">
						<span class="overlay-content"> <span class="h4">Project 4</span> </span>
					</span>
				</a>
				<div class="item-details bg-noise">
					<h4 class="item-title">
						<a href="#">Project 4</a>
					</h4>
					<a href="#" class="btn btn-more"><i class="fa fa-plus"></i>Read more</a>
				</div>
			</div>

			<div class="item">
				<a href="#" class="overlay-wrapper">
					<img src="{{ asset('img/showcase/project5.png') }}" alt="Project 5 image" class="img-responsive underlay">
					<span class="overlay">
						<span class="overlay-content"> <span class="h4">Project 5</span> </span>
					</span>
				</a>
				<div class="item-details bg-noise">
					<h4 class="item-title">
						<a href="#">Project 5</a>
					</h4>
					<a href="#" class="btn btn-more"><i class="fa fa-plus"></i>Read more</a>
				</div>
			</div>
			
			<div class="item">
				<a href="#" class="overlay-wrapper">
					<img src="{{ asset('img/showcase/project6.png') }}" alt="Project 6 image" class="img-responsive underlay">
					<span class="overlay">
						<span class="overlay-content"> <span class="h4">Project 6</span> </span>
					</span>
				</a>
				<div class="item-details bg-noise">
					<h4 class="item-title">
						<a href="#">Project 6</a>
					</h4>
					<a href="#" class="btn btn-more"><i class="fa fa-plus"></i>Read more</a>
				</div>
			</div>
	  	</div>
	</div>
</div>