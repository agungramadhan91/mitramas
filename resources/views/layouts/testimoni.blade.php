<div class="block block-pd-sm block-bg-grey-dark block-bg-overlay block-bg-overlay-6 text-center" data-block-bg-img="https://picjumbo.imgix.net/HNCK1088.jpg?q=40&amp;w=1650&amp;sharp=30" data-stellar-background-ratio="0.3">
	  <h2 class="h-xlg h1 m-a-0">
		  <span data-counter-up>100,000,0</span>s
		</h2>
	  <h3 class="h-lg m-t-0 m-b-lg">
		  Of Happy Customers!
		</h3>
	  <p>
		<a href="#" class="btn btn-more btn-lg i-right">Join them today! <i class="fa fa-angle-right"></i></a>
	  </p>
	</div>
	<!--Customer testimonial & Latest Blog posts-->
	<div class="testimonials block-contained">
	  <div class="row">
		<!--Customer testimonial-->
		<div class="col-md-12 m-b-lg">
		  <h3 class="block-title">
			  Testimonials
			</h3>
		  <blockquote>
			<p>Our productivity &amp; sales are up! Customers are happy &amp; we couldn't be happier with this product!</p>
			<img src="{{ asset('img/misc/charles-quote.png') }}" alt="Charles Spencer Chaplin">
			<small>
				<strong>Charles Chaplin</strong>
				<br>
				British comic actor
			  </small>
		  </blockquote>
		</div>
	  </div>
	</div>