<!-- Services -->
<div class="services block block-bg-gradient block-border-bottom">
  	<div class="container">
		<h2 class="block-title">
			Layanan Kami
	  </h2>
		<div class="row">
			<div class="col-md-4 text-center">
				<span class="fa-stack fa-5x">
					<i class="fa fa-circle fa-stack-2x text-primary"></i>
					<i class="fa fa-pencil fa-stack-1x fa-inverse"></i> 
				</span>
				<h4 class="text-weight-strong">
					Fabrikasi
				</h4>
				<p>
					Fabrikasi logam adalah suatu proses produksi logam yang meliputi antara lain rekayasa, pemotongan, pembentukan, penyambungan, perakitan atau pengerjaan akhir.
				</p>
				<p><a href="{{ route('fabrikasi') }}" class="btn btn-more i-right">Pelajari lebih lanjut <i class="fa fa-angle-right"></i></a></p>
			</div>
			<div class="col-md-4 text-center">
				<span class="fa-stack fa-5x">
					<i class="fa fa-circle fa-stack-2x text-primary"></i>
					<i class="fa fa-cogs fa-stack-1x fa-inverse"></i> 
				</span>
				<h4 class="text-weight-strong">
					Machining
				</h4>
				<p>
					Machining adalah proses pembuatan benda kerja dengan perautan (menghilangkan material yang tidak diinginkan dari benda kerja dalam bentuk chip).
				</p>
				<p><a href="{{ route('machining') }}" class="btn btn-more i-right">Pelajari lebih lanjut <i class="fa fa-angle-right"></i></a></p>
			</div>
			<div class="col-md-4 text-center">
				<span class="fa-stack fa-5x">
					<i class="fa fa-circle fa-stack-2x text-primary"></i>
					<i class="fa fa-group fa-stack-1x fa-inverse"></i> 
				</span>
				<h4 class="text-weight-strong">
					General Trading
				</h4>
				<p>
					Trading companies are businesses working with different kinds of products which are sold for consumer, business or government purposes. Trading companies buy a specialized range of products, maintain a stock or a shop, and deliver products to customers.
				</p>
				<p><a href="{{ route('trading') }}" class="btn btn-more i-right">Pelajari lebih lanjut <i class="fa fa-angle-right"></i></a></p>
			</div>
		</div>
  	</div>
</div>