<footer id="footer" class="block block-bg-grey-dark" data-block-bg-img="{{ asset('img/bg_footer-map.png') }}" data-stellar-background-ratio="0.4">
	<div class="container">
	  	<div class="row" id="contact">
			<div class="col-md-4">
			  <address>
				  <strong>PT. Mitramas Muda Mandiri</strong>
				  <br>
				  <i class="fa fa-map-pin fa-fw text-primary"></i> Jl. Pertamina, Dusun Kalijati, RT 007 RW 004, Desa Margamulya, Kecamatan Teluk Jambe Barat, Kabupaten Karawang
				  <br>
				  <a href="tel:628892335011"><i class="fa fa-phone fa-fw text-primary"></i> +62889-2335-011</a>
				  <br>
				  <a href="mailto:mitramasmuda@gmail.com"><i class="fa fa-envelope-o fa-fw text-primary"></i> mitramasmuda@gmail.com</a>
				  <br>
				</address>
			</div>
	  	</div>

	  <div class="row subfooter">
		<!--@todo: replace with company copyright details-->
		<div class="col-md-7">
		  <p>Escopy Team © 2017</p>
		</div>
		<div class="col-md-5">
		  <div class="credits pull-right">
			<!--
			  All the links in the footer should remain intact.
			  You can delete the links only if you purchased the pro version.
			  Licensing information: https://bootstrapmade.com/license/
			  Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Flexor
			-->
			<a href="https://bootstrapmade.com/">by BootstrapMade.com</a> 
		  </div>
		</div>
	  </div>

	  <a href="#top" class="scrolltop">Top</a>

	</div>
  </footer>