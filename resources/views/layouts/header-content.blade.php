<!--Change the background class to alter background image, options are: benches, boots, buildings, city, metro -->
<div id="background-wrapper" class="buildings" data-stellar-background-ratio="{{ Request::path() == '/' ? '0.1':'0.8>' }}">

	<!-- ======== @Region: #navigation ======== -->
	<div id="navigation" class="wrapper">
	
		<!--Hidden Header Region-->
		<div class="header-hidden collapse">
			<div class="header-hidden-inner container">
				<div class="row">
					<div class="col-md-6">
						<div class="switcher">
							<div class="cols">
								Change Colours:
								<br>
								<a href="#orange" class="colour orange active" title="Orange">Orange</a> <a href="#green" class="colour green " title="Green">Green</a> <a href="#blue" class="colour blue " title="Blue">Blue</a> <a href="#lavender" class="colour lavender " title="Lavender">Lavender</a>
							</div>
							<div class="cols">
								Change Language:
								<br>
								<a href="#orange" class="colour orange active" title="Orange">Orange</a> <a href="#green" class="colour green " title="Green">Green</a> <a href="#blue" class="colour blue " title="Blue">Blue</a> <a href="#lavender" class="colour lavender " title="Lavender">Lavender</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!--Header & navbar-branding region-->
		<div class="header">
			<div class="header-inner container">
				<div class="row">
					
					<!--navbar-branding/logo - hidden image tag & site name so things like Facebook to pick up, actual logo set via CSS for flexibility -->
					<div class="col-md-8">
						<h1 class="">
							<img src="{{ asset('img/logo2.png') }}" alt="">Mitramas Muda Mandiri
						</h1>
						{{--  <a class="navbar-brand-here" href="index.html" title="Home">
						</a>  --}}
						{{--  <div class="navbar-slogan">
							Kepuasan anda<br> prioritas kami
						</div>  --}}
					</div>

					<!--header rightside-->
					<div class="col-md-4">
						
						<!--user menu-->
						<ul class="list-inline user-menu pull-right">
							@if(Auth::check())
								<li><a href="{{ route('create') }}">{{ Auth::user()->name }}</a></li>
								<li>
									<a href="{{ route('logout') }}"
										onclick="event.preventDefault();
												document.getElementById('logout-form').submit();">
										Logout
									</a>

									<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
										{{ csrf_field() }}
									</form>
								</li>
							@else
								<li><a class="btn btn-primary btn-hh-trigger" role="button" data-toggle="collapse" data-target=".header-hidden">Open</a></li>
							@endif
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="navbar navbar-default">

				<!--mobile collapse menu button-->
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>

				<!--social media icons-->
				<div class="navbar-text social-media social-media-inline pull-right">
					<!--@todo: replace with company social media details-->
					<!-- <a href="#"><i class="fa fa-twitter"></i></a>
					<a href="#"><i class="fa fa-facebook"></i></a>
					<a href="#"><i class="fa fa-linkedin"></i></a>
					<a href="#"><i class="fa fa-google-plus"></i></a> -->
				</div>

				<!--everything within this div is collapsed on mobile-->
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav" id="main-menu">
						<li class="icon-link">
							<a href="{{ url('/') }}"><i class="fa fa-home"></i></a>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Company<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li class="dropdown-header">Legalitas Perusahaan</li>
								<li><a href="{{ route('about') }}" tabindex="-1" class="menu-item">Tentang</a></li>
								<li><a href="{{ route('akta') }}" tabindex="-1" class="menu-item">Akta Perusahaan</a></li>
								<li><a href="{{ route('legalitas') }}" tabindex="-1" class="menu-item">Legalitas</a></li>
							</ul>
						</li>
						<li><a href="{{ route('fabrikasi') }}">Fabrikasi</a></li>
						<li><a href="{{ route('machining') }}">Machining</a></li>
						<li><a href="{{ route('trading') }}">General Trading</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	@yield('header')
</div>