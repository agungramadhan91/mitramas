<div class="col-md-6">
    <div class="form-group {!! $errors->has('type') ? 'has-error' : '' !!}">
        {!! Form::label('type_label', 'Type', ['class'=>'col-md-2 control-label']) !!}
        <div class="col-md-10">
            {!! Form::select('type', [ 'fabrikasi'=>'Fabrikasi', 'machining'=>'Machining', 'trading'=>'General Trading' ], null, ['class'=>'form-control','placeholder'
            => '']) !!} {!! $errors->first('type', '
            <p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
        {!! Form::label('name', 'Nama', ['class'=>'col-md-2 control-label']) !!}
        <div class="col-md-10">
            {!! Form::text('name', null, ['class'=>'form-control']) !!} {!! $errors->first('name', '
            <p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
        {!! Form::label('desc', 'Deskripsi', ['class'=>'col-md-2 control-label']) !!}
        <div class="col-md-10">
            {!! Form::textarea('description', null, ['class'=>'form-control','rows'=>'3']) !!} {!! $errors->first('description', '
            <p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-10 col-md-offset-2">
            @if(Request::path() == 'fabrikasi' || Request::path() == 'machining' || Request::path() == 'general-trading') {!! Form::submit('Simpan',
            ['class'=>'btn btn-primary']) !!} @else {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!} @endif
        </div>
    </div>

</div>
<div class="col-md-6">
    <div class="form-group{{ $errors->has('umum') ? ' has-error' : '' }}">
        {!! Form::label('photo_label', 'Tampak Umum', ['class'=>'col-md-4 control-label']) !!}
        <div class="col-md-8">
            {!! Form::file('umum') !!} @if (isset($images) && $image->umum)
            <br>
            <p>{!! Html::image(asset('img/uploaded'.$image->umum), null, ['class'=>'img-responsive']) !!}</p>
            @endif {!! $errors->first('umum', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('depan') ? ' has-error' : '' }}">
        {!! Form::label('photo_label', 'Tampak Depan', ['class'=>'col-md-4 control-label']) !!}
        <div class="col-md-8">
            {!! Form::file('depan') !!} @if (isset($images) && $image->depan)
            <br>
            <p>{!! Html::image(asset('img/uploaded'.$image->depan), null, ['class'=>'img-responsive']) !!}</p>
            @endif {!! $errors->first('depan', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('belakang') ? ' has-error' : '' }}">
        {!! Form::label('photo_label', 'Tampak Belakang', ['class'=>'col-md-4 control-label']) !!}
        <div class="col-md-8">
            {!! Form::file('belakang') !!} @if (isset($images) && $image->belakang)
            <br>
            <p>{!! Html::image(asset('img/uploaded'.$image->belakang), null, ['class'=>'img-responsive']) !!}</p>
            @endif {!! $errors->first('belakang', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('atas') ? ' has-error' : '' }}">
        {!! Form::label('photo_label', 'Tampak Atas', ['class'=>'col-md-4 control-label']) !!}
        <div class="col-md-8">
            {!! Form::file('atas') !!} @if (isset($images) && $image->atas)
            <br>
            <p>{!! Html::image(asset('img/uploaded'.$image->atas), null, ['class'=>'img-responsive']) !!}</p>
            @endif {!! $errors->first('atas', '
            <p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('bawah') ? ' has-error' : '' }}">
        {!! Form::label('photo_label', 'Tampak Bawah', ['class'=>'col-md-4 control-label']) !!}
        <div class="col-md-8">
            {!! Form::file('bawah') !!} @if (isset($images) && $image->bawah)
            <br>
            <p>{!! Html::image(asset('img/uploaded'.$image->depan), null, ['class'=>'img-responsive']) !!}</p>
            @endif {!! $errors->first('bawah', '
            <p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('kiri') ? ' has-error' : '' }}">
        {!! Form::label('photo_label', 'Tampak kiri', ['class'=>'col-md-4 control-label']) !!}
        <div class="col-md-8">
            {!! Form::file('kiri') !!} @if (isset($images) && $image->kiri)
            <br>
            <p>{!! Html::image(asset('img/uploaded'.$image->kiri), null, ['class'=>'img-responsive']) !!}</p>
            @endif {!! $errors->first('kiri', '
            <p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('kanan') ? ' has-error' : '' }}">
        {!! Form::label('photo_label', 'Tampak Kanan', ['class'=>'col-md-4 control-label']) !!}
        <div class="col-md-8">
            {!! Form::file('kanan') !!} @if (isset($images) && $image->kanan)
            <br>
            <p>{!! Html::image(asset('img/uploaded'.$image->kanan), null, ['class'=>'img-responsive']) !!}</p>
            @endif {!! $errors->first('kanan', '
            <p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>