@extends('layouts.app')

@section('content')

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<div class="row">
		{!! Form::model($business, ['url' => route('update', $business->id),'method' => 'put', 'files'=>'true', 'class'=>'form-horizontal']) !!}
			
			<div class="panel panel-default">
				<div class="panel-heading">
					<h2 class="panel-title">Edit File {{ ucfirst($business->type) }}</h2>
				</div>
				<div class="panel-body">    
					
					@include('admin._form')
					
				</div>
			</div>
			
		{!! Form::close() !!}
	</div>
</div>
@endsection