<div class="row" id="contact">
    {{--  <div class="col-md-6">
        <header>
            <h2><span>Hai Admin!</span> Selamat datang :)</h2>
        </header>
    </div>  --}}
    {!! Form::open(['url' => route('store'),'method' => 'post', 'files'=>'true', 'class'=>'form-horizontal']) !!}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title">Tambah File {{ ucfirst(Request::path()) }}</h2>
            </div>
            <div class="panel-body">    
                
                @include('admin._form')
                
            </div>
        </div>
        
    {!! Form::close() !!}
</div>