@extends('layouts.app')

@section('links')
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
    <link rel="stylesheet" href="{{ asset('css/thumbnail-gallery.css') }}">
@endsection

@section('header')
	<div class="hero" id="highlighted">
		<div class="inner">
			<div id="highlighted-slider" class="container">
				<div class="item-slider" data-toggle="owlcarousel" data-owlcarousel-settings='{"singleItem":true, "navigation":true, "transitionStyle":"fadeUp"}'>

					@foreach($businesses->unique('type') as $business)
						<div class="item">
							<div class="row">
								<div class="col-md-6 col-md-push-6 item-caption">
									<h2 class="h1 text-weight-light">
										@if($business->type == 'trading')
											<span class="text-primary">General {{ ucfirst($business->type) }}</span>
										@else
											<span class="text-primary">{{ ucfirst($business->type) }}</span>
										@endif
									</h2>
									<h4>{{ $business->description }}</h4>
									<a href="{{ route($business->type) }}" class="btn btn-primary">Kunjungi</a>
								</div>
								<div class="col-md-6 col-md-pull-6 hidden-xs">
									{!! Html::image(asset('img/uploaded/'.$business->image->umum), null, ['class'=>'center-block img-responsive']) !!}
								</div>
							</div>
						</div>
					@endforeach
					
				</div>
			</div><br>
		</div>
	</div>
@endsection

@section('content')
	<!-- Mission Statement -->
	<div class="mission text-center block block-pd-sm block-bg-noise">
		<div class="container">
			<h2 class="text-shadow-white">
				Kualitas dan kepuasan pelanggan menjadi prioritas kami.
				<a href="{{ route('about') }}" class="btn btn-primary">Lanjutkan membaca</a>
			</h2>
		</div>
	</div>

	{{--  @include('layouts.showcase')  --}}

	@include('layouts.service')
@endsection

@section('footer')

@endsection