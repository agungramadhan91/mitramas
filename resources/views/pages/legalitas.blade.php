@extends('layouts.app')

@section('links')
	@if(Request::path() == 'legalitas')
		<li class="{{ Request::path() == 'about' ? 'active':'' }}">
			<a href="{{ route('about') }}" class="first">
				Tentang Kami
				<small>Sekilas pandang mengenai perusahaan</small>
			</a>
		</li>
		<li class="{{ Request::path() == 'akta' ? 'active':'' }}">
			<a href="{{ route('akta') }}">
				Akta
				<small>Berdirinya perusahaan Kami</small>
			</a>
		</li>
		<li class="{{ Request::path() == 'legalitas' ? 'active':'' }}">
			<a href="{{ route('legalitas') }}">
				Legalitas
				<small>Legalitas perusahaan Kami</small>
			</a>
		</li>
	@endif
@endsection

@section('content')
	<div class="page-header">
        <h1>
            Legalitas Perusahaan
            <small>Landasan huukum perusahaan Kami</small>
        </h1>
    </div>
    <div class="block block-border-bottom-grey block-pd-sm">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<section class="blog-content">
				<img src="img/files/siup.jpg" alt="" class="img-responsive"/>
			</section>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<section class="blog-content">
				<img src="img/files/npwp.jpg" alt="" class="img-responsive"/>
			</section>
		</div>
		<div class="clearfix"></div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<section class="blog-content">
				<img src="img/files/skt.jpg" alt="" class="img-responsive"/>
			</section>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<section class="blog-content">
				<img src="img/files/tdp.jpg" alt="" class="img-responsive"/>
			</section>
		</div>
		<div class="clearfix"></div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<section class="blog-content">
				<img src="img/files/sppkp.jpg" alt="" class="img-responsive"/>
			</section>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<section class="blog-content">
				<img src="img/files/skdp.jpg" alt="" class="img-responsive"/>
			</section>
		</div>
	</div>
@endsection