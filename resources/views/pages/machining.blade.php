@extends('layouts.app')

@section('header')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
    <link rel="stylesheet" href="{{ asset('css/thumbnail-gallery.css') }}">
@endsection

@section('content')
	<!--Showcase-->
<div class="showcase block block-border-bottom-grey">
	<div class="container">
        
        @if(Auth::check())
            @include('admin.create')
        @endif

		<h2 class="block-title">
			Showcase Machining
		</h2>
		<p>
			Machining adalah proses pembuatan benda kerja dengan perautan (menghilangkan material yang tidak diinginkan dari benda kerja dalam bentuk chip).
		</p>
    
        <div class="tz-gallery">
            <div class="row">

                @if(count($businesses) > 0)
                    @foreach($businesses->where('type','machining') as $business)
                        <div class="col-sm-6 col-md-4">
                            {{--  <div class="img" style="background-image:url({{ asset('image/'.$business->photo) }})"></div>  --}}
                            <div class="thumbnail">
                                <a class="lightbox" href="{{ asset('img/uploaded/'.$business->image->umum) }}">
                                    <div class="wrapper-image" style="background-image: url({{ asset('img/uploaded/'.$business->image->umum) }});"></div>
                                    {{--  {!! Html::image(asset('image/'.$business->photo), null, ['class'=>'img-responsive underlay']) !!}  --}}
                                </a>
                                <div class="caption">
                                    <h3>
                                        <a href="{{ Auth::check() ? route('edit', $business->id) : '#'}}">
                                            {{ ucfirst($business->name) }}
                                        </a>
                                    </h3>
                                    <p>{{ ucfirst($business->description) }}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif

            </div>
        </div>
	</div>
</div>
@endsection

@section('footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
    <script>
        baguetteBox.run('.tz-gallery');
    </script>
@endsection