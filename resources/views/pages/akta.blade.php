@extends('layouts.app')

@section('links')
	@if(Request::path() == 'akta')
		<li class="{{ Request::path() == 'about' ? 'active':'' }}">
			<a href="{{ route('about') }}" class="first">
				Tentang Kami
				<small>Sekilas pandang mengenai perusahaan</small>
			</a>
		</li>
		<li class="{{ Request::path() == 'akta' ? 'active':'' }}">
			<a href="{{ route('akta') }}">
				Akta Perusahaan
				<small>Berdirinya perusahaan Kami</small>
			</a>
		</li>
		<li class="{{ Request::path() == 'legalitas' ? 'active':'' }}">
			<a href="{{ route('legalitas') }}">
				Legalitas
				<small>Legalitas perusahaan Kami</small>
			</a>
		</li>
	@endif
@endsection

@section('content')
	<div class="page-header">
        <h1>
            Akta Perusahaan
            <small>Berdirinya perusahaan Kami</small>
        </h1>
    </div>
    <div class="block block-border-bottom-grey block-pd-sm">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<section class="blog-content">
				<a href="blog-details.html">
				<figure>
					<img src="img/files/Depan.jpg" alt="" class="img-responsive"/>
				</figure>
				</a>
			</section>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<section class="blog-content">
				<a href="blog-details.html">
				<figure>
					<img src="img/files/Lembar 1.jpg" alt="" class="img-responsive"/>
				</figure>
				</a>
			</section>
		</div>
		<div class="clearfix"></div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<section class="blog-content">
				<a href="blog-details.html">
				<figure>
					<img src="img/files/Lembar 2.jpg" alt="" class="img-responsive"/>
				</figure>
				</a>
			</section>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<section class="blog-content">
				<a href="blog-details.html">
				<figure>
					<img src="img/files/Lembar 3.jpg" alt="" class="img-responsive"/>
				</figure>
				</a>
			</section>
		</div>
		<div class="clearfix"></div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<section class="blog-content">
				<a href="blog-details.html">
				<figure>
					<img src="img/files/Lembar 4.jpg" alt="" class="img-responsive"/>
				</figure>
				</a>
			</section>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<section class="blog-content">
				<a href="blog-details.html">
				<figure>
					<img src="img/files/Lembar 5.jpg" alt="" class="img-responsive"/>
				</figure>
				</a>
			</section>
		</div>
		<div class="clearfix"></div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<section class="blog-content">
				<a href="blog-details.html">
				<figure>
					<img src="img/files/Lembar 6.jpg" alt="" class="img-responsive"/>
				</figure>
				</a>
			</section>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<section class="blog-content">
				<a href="blog-details.html">
				<figure>
					<img src="img/files/Lembar 7.jpg" alt="" class="img-responsive"/>
				</figure>
				</a>
			</section>
		</div>
		<div class="clearfix"></div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<section class="blog-content">
				<a href="blog-details.html">
				<figure>
					<img src="img/files/Lembar 8.jpg" alt="" class="img-responsive"/>
				</figure>
				</a>
			</section>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<section class="blog-content">
				<a href="blog-details.html">
				<figure>
					<img src="img/files/Lembar 9.jpg" alt="" class="img-responsive"/>
				</figure>
				</a>
			</section>
		</div>
    </div>
    
@endsection