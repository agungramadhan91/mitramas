@extends('layouts.app')

@section('content')
	<div class="row">
    	<section class="col-xs-12 col-sm-6 col-md-6 col-lg-6 ">
        	<article role="pge-title-content">
            	<header>
                	<h2>
                		<span>MITRAMAS</span>
                		<span>MUDA</span>
                		<span>MANDIRI</span>
                	</h2>
                </header>
                <p>
                	Merupakan perusahaan berbadan hukum yang bergerak dibidang
                	<a href="{{ route('fabrikasi') }}">Fabrikasi</a>,
                	<a href="{{ route('machining') }}">Machining</a>, dan
                	<a href="{{ route('trading') }}">General Trading</a>.
                </p>
            </article>
        </section>
        
        <section class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        	<article class="about-content">
            	<p>
            		Latar belakang pendiriannya didasari oleh keinginan yang kuat untuk mandiri, membangun kerjasama, dan dikembangkan dengan semangat serta kemauan yang kuat untuk memberikan pelayanan yang terbaik kepada pelanggan.
            	</p>

                <p>Berbekal pengetahuan, keterampilan, dan pengalaman yang ada. Kami bertekad selalu memberikan pelayanan sepenuhnya untuk kepentingan perkembangan dan pertumbuhan usaha pelanggan.</p>

                <p>Kualitas dan kepuasan pelanggan menjadi prioritas kami.</p>
            </article>
        </section>
        
        <div class="clearfix"></div><br><br><br><br>

        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <header role="bog-header" class="bog-header text-center">
                <h3><span>VISI</span></h3>
                <h3>Menjadi perusahaan kelas dunia</h3>
            </header>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
        	<header role="bog-header" class="bog-header text-center">
                <h3><span>MISI</span></h3>
                <h3>Menjual barang yang berkualitas, berguna, dan bermutu</h3>
            </header>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
        	<header role="bog-header" class="bog-header text-center">
                <h3><span>KOMITMEN</span></h3>
                <h3>Memberikan pelayanan yang baik dan pengiriman yang tepat waktu</h3>
            </header>
        </div>

        <div class="clearfix"></div>
        
    	<div class="thumbnails-pan">
        	<section class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
            	<figure>
                	<img src="images/files/Acount Bank.jpg" class="img-responsive"/>
                	<figcaption>
                    	<h3>AKUN BANK</h3>
                        <h5>Mitramas Muda Mandiri</h5>
                        <h5>mandiri - 120-0004902131</h5>
                    </figcaption>
                </figure>
            </section>

            <section class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
            	<figure>
                	<a href="{{ route('akta') }}"><img src="images/files/Depan.jpg" class="img-responsive"/></a>
                	<figcaption>
                    	<h3>AKTA</h3>
                        <h5>Cover depan dan berisi 9 halaman</h5>
                    </figcaption>
                </figure>
            </section>

            <section class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
            	<figure>
                	<a href="{{ route('legalitas') }}"><img src="images/files/siup.jpg" class="img-responsive"/></a>
                	<figcaption>
                    	<h3>LEGALITAS</h3>
                        <h5>SIUP, NPWP, SKT, TDP, SPPKP, SKDP, dan Keputusan Menteri Hukum dan HAM</h5>
                    </figcaption>
                </figure>
            </section>
        </div>
    </div>
@endsection