@extends('layouts.app')

@section('content')
	<div class="row">
    	<div class="row">
        	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 ">
            	<article role="pge-title-content" class="blog-header">
                	<header>
                    	<h2><span>LEGALITAS</span>PT.MITRAMAS MUDA MANDIRI</h2>
                    </header>
                    <p>
	                	Merupakan perusahaan berbadan hukum yang bergerak dibidang
	                	<a href="{{ route('fabrikasi') }}">Fabrikasi</a>,
	                	<a href="{{ route('machining') }}">Machining</a>, dan
	                	<a href="{{ route('trading') }}">General Trading</a>.
	                </p>
                </article>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            	<ul class="grid-lod effect-2" id="grid">
            		<li>
                    	<section class="blog-content">
                            <figure>
                                <img src="images/files/npwp.jpg" alt="" class="img-responsive"/>
                            </figure>
                            </a>
                        </section>
                	</li>
                </ul>
            </div>
            
            <div class="clearfix"></div>
            <div class="thumbnails-pan">
	        	<section class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
	            	<figure>
	                	<img src="images/files/siup.jpg" class="img-responsive"/>
	                	<figcaption>
	                    	<h3>LEGALITAS</h3>
	                        <h5>SIUP</h5>
	                    </figcaption>
	                </figure>
	            </section>
	            <section class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
	            	<figure>
	                	<img src="images/files/skdp.jpg" class="img-responsive"/>
	                	<figcaption>
	                    	<h3>LEGALITAS</h3>
	                        <h5>SKDP</h5>
	                    </figcaption>
	                </figure>
	            </section>
	            <section class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
	            	<figure>
	                	<img src="images/files/skt.jpg" class="img-responsive"/>
	                	<figcaption>
	                    	<h3>LEGALITAS</h3>
	                        <h5>SKT</h5>
	                    </figcaption>
	                </figure>
	            </section>
	        </div>
	        <div class="clearfix"></div>
	        <div class="thumbnails-pan">
	        	<section class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
	            	<figure>
	                	<img src="images/files/sppkp.jpg" class="img-responsive"/>
	                	<figcaption>
	                    	<h3>LEGALITAS</h3>
	                        <h5>SPPKP</h5>
	                    </figcaption>
	                </figure>
	            </section>
	            <section class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
	            	<figure>
	                	<img src="images/files/tdp.jpg" class="img-responsive"/>
	                	<figcaption>
	                    	<h3>LEGALITAS</h3>
	                        <h5>TDP</h5>
	                    </figcaption>
	                </figure>
	            </section>
	            <section class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
	            	<figure>
	                	<img src="images/files/keputusan.jpg" class="img-responsive"/>
	                	<figcaption>
	                    	<h3>LEGALITAS</h3>
	                        <h5>Keputusan Menteri Hukum dan HAM</h5>
	                    </figcaption>
	                </figure>
	            </section>
	        </div>

        </div>
    </div>
@endsection