@extends('layouts.app')

@section('content')
	<div class="row">
    	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 ">
        	<article role="pge-title-content" class="blog-header">
            	<header>
                	<h2><span>GENERAL</span><span>TRADING</span>Contoh barang yang pernah kami kerjakan.</h2>
                </header>
            </article>

            <ul class="grid-lod effect-2" id="grid">
            	@foreach($businesses as $business)
            		@if($business->id % 2 == 1)
		                <li>
		                    <section class="blog-content">
		                    	<a href="{{ Auth::check() ? route('edit', $business->id) : '#'}}">
			                        <figure>
			                            {!! Html::image(asset('image/'.$business->photo), null, ['class'=>'img-responsive']) !!}
			                        </figure>
			                        <article>{{ $business->description }}</article>
		                        </a>
		                    </section>
		                </li>
	                @endif
                @endforeach
            </ul>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        	<ul class="grid-lod effect-2" id="grid">
        		@foreach($businesses as $business)
            		@if($business->id % 2 == 0)
		                <li>
		                    <section class="blog-content">
		                    	<a href="{{ Auth::check() ? route('edit', $business->id) : '#'}}">
			                        <figure>
			                            {!! Html::image(asset('image/'.$business->photo), null, ['class'=>'img-responsive']) !!}
			                        </figure>
			                        <article>{{ $business->description }}</article>
		                        </a>
		                    </section>
		                </li>
	                @endif
                @endforeach
        		
                <li>

                    <section class="blog-content">

                    	<a href="#">

                        <figure>

                            <div class="post-date">

                                <span>24</span> July 2016

                            </div>

                            <img src="images/blog-images/blog-3.jpg" alt="" class="img-responsive"/>

                        </figure>

                        </a>

                        <article>

                            This is a sample news post title content or sample post heading.

                        </article>

                    </section>

                </li>

                <li>

                	<section class="blog-content">

                    	<a href="#">

                        <figure>

                            <div class="post-date">

                                <span>24</span> July 2016

                            </div>

                            <img src="images/blog-images/blog-5.jpg" alt="" class="img-responsive"/>

                        </figure>

                        </a>

                        <article>

                            This is a sample news post title content or sample post heading.

                        </article>

                    </section>

                </li>
            </ul>
        </div>
    </div>
@endsection