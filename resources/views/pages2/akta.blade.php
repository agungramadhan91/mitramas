@extends('layouts.app')

@section('content')
	<div class="row">
    	<div class="row">
        	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 ">
            	<article role="pge-title-content" class="blog-header">
                	<header>
                    	<h2><span>AKTA</span>PT.MITRAMAS MUDA MANDIRI</h2>
                    </header>
                    <p>
	                	Merupakan perusahaan berbadan hukum yang bergerak dibidang
	                	<a href="{{ route('fabrikasi') }}">Fabrikasi</a>,
	                	<a href="{{ route('machining') }}">Machining</a>, dan
	                	<a href="{{ route('trading') }}">General Trading</a>.
	                </p>
                </article>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            	<ul class="grid-lod effect-2" id="grid">
            		<li>
                    	<section class="blog-content">
                        	<a href="blog-details.html">
                            <figure>
                                <img src="images/files/Depan.jpg" alt="" class="img-responsive"/>
                            </figure>
                            </a>
                        </section>
                	</li>
                </ul>
            </div>

            <div class="thumbnails-pan">
	        	<section class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
	            	<figure>
	                	<img src="images/files/Lembar 1.jpg" class="img-responsive"/>
	                	<figcaption>
	                    	<h3>AKTA</h3>
	                        <h5>Lembar 1</h5>
	                    </figcaption>
	                </figure>
	            </section>
	            <section class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
	            	<figure>
	                	<img src="images/files/Lembar 2.jpg" class="img-responsive"/>
	                	<figcaption>
	                    	<h3>AKTA</h3>
	                        <h5>Lembar 2</h5>
	                    </figcaption>
	                </figure>
	            </section>
	            <section class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
	            	<figure>
	                	<img src="images/files/Lembar 3.jpg" class="img-responsive"/>
	                	<figcaption>
	                    	<h3>AKTA</h3>
	                        <h5>Lembar 3</h5>
	                    </figcaption>
	                </figure>
	            </section>
	        </div>
	        <div class="clearfix"></div>
	        <div class="thumbnails-pan">
	        	<section class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
	            	<figure>
	                	<img src="images/files/Lembar 4.jpg" class="img-responsive"/>
	                	<figcaption>
	                    	<h3>AKTA</h3>
	                        <h5>Lembar 4</h5>
	                    </figcaption>
	                </figure>
	            </section>
	            <section class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
	            	<figure>
	                	<img src="images/files/Lembar 5.jpg" class="img-responsive"/>
	                	<figcaption>
	                    	<h3>AKTA</h3>
	                        <h5>Lembar 5</h5>
	                    </figcaption>
	                </figure>
	            </section>
	            <section class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
	            	<figure>
	                	<img src="images/files/Lembar 6.jpg" class="img-responsive"/>
	                	<figcaption>
	                    	<h3>AKTA</h3>
	                        <h5>Lembar 6</h5>
	                    </figcaption>
	                </figure>
	            </section>
	        </div>
	        <div class="clearfix"></div>
	        <div class="thumbnails-pan">
	        	<section class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
	            	<figure>
	                	<img src="images/files/Lembar 7.jpg" class="img-responsive"/>
	                	<figcaption>
	                    	<h3>AKTA</h3>
	                        <h5>Lembar 7</h5>
	                    </figcaption>
	                </figure>
	            </section>
	            <section class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
	            	<figure>
	                	<img src="images/files/Lembar 8.jpg" class="img-responsive"/>
	                	<figcaption>
	                    	<h3>AKTA</h3>
	                        <h5>Lembar 8</h5>
	                    </figcaption>
	                </figure>
	            </section>
	            <section class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
	            	<figure>
	                	<img src="images/files/Lembar 9.jpg" class="img-responsive"/>
	                	<figcaption>
	                    	<h3>AKTA</h3>
	                        <h5>Lembar 9</h5>
	                    </figcaption>
	                </figure>
	            </section>
	        </div>

        </div>
    </div>
@endsection