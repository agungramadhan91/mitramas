<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Image;

class Business extends Model
{//ini dari lokal ke git
    protected $fillable = ['type','name','description'];

    public function image()
    {
        return $this->hasOne('App\Image');
    }
}
