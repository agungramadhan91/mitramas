<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Business;

class FrontController extends Controller
{
    public function landing()
    {
        $businesses = Business::all();
        // $businesses_random = Business::all()->random(5);
        // return view('layouts.app',compact('businesses', 'businesses_random'));
    	return view('pages.landing',compact('businesses'));
    }

    public function akta()
    {
    	return view('pages.akta');	
    }

    public function legalitas()
    {
    	return view('pages.legalitas');	
    }

    public function about()
    {
    	return view('pages.about');	
    }

    public function fabrikasi()
    {
        $businesses = Business::where('type', 'fabrikasi')->get();
    	return view('pages.fabrikasi', compact('businesses'));	
    }

    public function machining()
    {
    	$businesses = Business::where('type', 'machining')->get();
        return view('pages.machining', compact('businesses'));
    }

    public function trading()
    {
    	$businesses = Business::where('type', 'trading')->get();
        return view('pages.trading', compact('businesses'));
    }

    public function contact()
    {
    	return view('pages.contact');	
    }
}
