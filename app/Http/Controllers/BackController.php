<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Business;
use App\Image;

class BackController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
    	// $businesses = Business::all();
        // return view('admin.create', compact('businesses'));
        
        return redirect()->route('landing');
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
            'type'      => 'required|in:fabrikasi,machining,trading',
            'name' 	    => 'required',
            'umum'     	=> 'required|image',
            'depan'     => 'image',
            'belakang'  => 'image',
            'atas'     	=> 'image',
            'bawah'     => 'image',
            'kiri'     	=> 'image',
            'kanan'     => 'image',
        ]);

        $business = Business::create($request->all());
        
        $image = new Image();
        $image->business_id = $business->id;

        if ($request->hasFile('umum')) {
            $uploaded_photo = $request->file('umum');
            $extension = $uploaded_photo->getClientOriginalExtension();
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img/uploaded';
            $uploaded_photo->move($destinationPath, $filename);
            $image->umum = $filename;
        }

        if ($request->hasFile('depan')) {
            $uploaded_photo = $request->file('depan');
            $extension = $uploaded_photo->getClientOriginalExtension();
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img/uploaded';
            $uploaded_photo->move($destinationPath, $filename);
            $image->depan = $filename;
        }

        if ($request->hasFile('belakang')) {
            $uploaded_photo = $request->file('belakang');
            $extension = $uploaded_photo->getClientOriginalExtension();
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img/uploaded';
            $uploaded_photo->move($destinationPath, $filename);
            $image->belakang = $filename;
        }

        if ($request->hasFile('atas')) {
            $uploaded_photo = $request->file('atas');
            $extension = $uploaded_photo->getClientOriginalExtension();
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img/uploaded';
            $uploaded_photo->move($destinationPath, $filename);
            $image->atas = $filename;
        }

        if ($request->hasFile('bawah')) {
            $uploaded_photo = $request->file('bawah');
            $extension = $uploaded_photo->getClientOriginalExtension();
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img/uploaded';
            $uploaded_photo->move($destinationPath, $filename);
            $image->bawah = $filename;
        }

        if ($request->hasFile('kiri')) {
            $uploaded_photo = $request->file('kiri');
            $extension = $uploaded_photo->getClientOriginalExtension();
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img/uploaded';
            $uploaded_photo->move($destinationPath, $filename);
            $image->kiri = $filename;
        }

        if ($request->hasFile('kanan')) {
            $uploaded_photo = $request->file('kanan');
            $extension = $uploaded_photo->getClientOriginalExtension();
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img/uploaded';
            $uploaded_photo->move($destinationPath, $filename);
            $image->kanan = $filename;
        }

        $image->save();

        // $um = $request->file('umum');
        // $dp = $request->file('depan');
        // $bk = $request->file('belakang');
        // $at = $request->file('atas');
        // $bw = $request->file('bawah');
        // $kr = $request->file('kiri');
        // $kn = $request->file('kanan');

        // $this->image_upload($um, $dp, $bk, $at, $bw, $kr, $kn);
        
        $type = $request->type;
        if ($type == 'fabrikasi') {
        	return redirect()->route('fabrikasi');
        } elseif ($type == 'machining') {
        	return redirect()->route('machining');
        } elseif ($type == 'trading') {
        	return redirect()->route('trading');
        }
    }

    public function edit(Request $request, $id)
    {	
    	$business = Business::find($id);
    	return view('admin.edit', compact('business'));
    }

    public function update(Request $request, $id)
    {
    	$this->validate($request, [
            'type'     		=> 'required',
            // 'description' 	=> 'required',
            'photo'     	=> 'image'
        ]);

    	$business = Business::find($id);
        if(!$business->update($request->all())) return redirect()->back();

        if ($request->hasFile('photo')) {
            $filename = null;
            $uploaded_photo = $request->file('photo');
            $extension = $uploaded_photo->getClientOriginalExtension();

            // membuat nama file random dengan extension
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'image';

            // memindahkan file ke folder public/img
            $uploaded_photo->move($destinationPath, $filename);

            // hapus photo lama, jika ada
            if ($business->photo) {
                $old_photo = $business->photo;
                $filepath = public_path() . DIRECTORY_SEPARATOR . 'image'
                    . DIRECTORY_SEPARATOR . $business->photo;

                try {
                    File::delete($filepath);
                } catch (FileNotFoundException $e) {
                    // File sudah dihapus/tidak ada
                }
            }

            // ganti field photo dengan photo yang baru
            $business->photo = $filename;
            $business->save();
        }

        // Session::flash("flash_notification", [
        //     "level"=>"success",
        //     "message"=>"Berhasil menyimpan $business->title"
        // ]);
        
        $type = $request->type;
        if ($type == 'fabrikasi') {
        	return redirect()->route('fabrikasi');
        } elseif ($type == 'machining') {
        	return redirect()->route('machining');
        } elseif ($type == 'trading') {
        	return redirect()->route('trading');
        }
    }

    public function image_upload($um, $dp, $bk, $at, $bw, $kr, $kn)
    {
        $image = new Image();

        if ($request->hasFile('umum')) {
            $uploaded_photo = $um;
            $extension = $uploaded_photo->getClientOriginalExtension();
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img/uploaded';
            $uploaded_photo->move($destinationPath, $filename);
            $image->umum = $filename;
            $image->save();
        }

        if ($request->hasFile('depan')) {
            $uploaded_photo = $dp;
            $extension = $uploaded_photo->getClientOriginalExtension();
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img/uploaded';
            $uploaded_photo->move($destinationPath, $filename);
            $image->depan = $filename;
            $image->save();
        }

        if ($request->hasFile('belakang')) {
            $uploaded_photo = $bk;
            $extension = $uploaded_photo->getClientOriginalExtension();
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img/uploaded';
            $uploaded_photo->move($destinationPath, $filename);
            $image->belakang = $filename;
            $image->save();
        }

        if ($request->hasFile('atas')) {
            $uploaded_photo = $at;
            $extension = $uploaded_photo->getClientOriginalExtension();
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img/uploaded';
            $uploaded_photo->move($destinationPath, $filename);
            $image->atas = $filename;
            $image->save();
        }

        if ($request->hasFile('bawah')) {
            $uploaded_photo = $bw;
            $extension = $uploaded_photo->getClientOriginalExtension();
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img/uploaded';
            $uploaded_photo->move($destinationPath, $filename);
            $image->bawah = $filename;
            $image->save();
        }

        if ($request->hasFile('kiri')) {
            $uploaded_photo = $kr;
            $extension = $uploaded_photo->getClientOriginalExtension();
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img/uploaded';
            $uploaded_photo->move($destinationPath, $filename);
            $image->kiri = $filename;
            $image->save();
        }

        if ($request->hasFile('kanan')) {
            $uploaded_photo = $kn;
            $extension = $uploaded_photo->getClientOriginalExtension();
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img/uploaded';
            $uploaded_photo->move($destinationPath, $filename);
            $image->kanan = $filename;
            $image->save();
        }
    }
}
