@extends('layouts.app')

@section('links')
	@if(Request::path() == 'about')
		<li class="{{ Request::path() == 'about' ? 'active':'' }}">
			<a href="{{ route('about') }}" class="first">
				Tentang Kami
				<small>Sekilas pandang mengenai perusahaan</small>
			</a>
		</li>
		<li class="{{ Request::path() == 'akta' ? 'active':'' }}">
			<a href="{{ route('akta') }}">
				Akta Perusahaan
				<small>Berdirinya perusahaan Kami</small>
			</a>
		</li>
		<li class="{{ Request::path() == 'legalitas' ? 'active':'' }}">
			<a href="{{ route('legalitas') }}">
				Legalitas
				<small>Legalitas perusahaan Kami</small>
			</a>
		</li>
	@endif
@endsection

@section('content')
	<div class="page-header">
        <h1>
            Tentang kami
            <small>Sekilas pandang mengenai perusahaan</small>
        </h1>
    </div>
    <div class="block block-border-bottom-grey block-pd-sm">
		<h3 class="block-title">
			Bagaimana Kami berdiri
		</h3>
		<img src="img/misc/about-us.png" alt="About us" class="img-responsive img-thumbnail pull-right m-l m-b">
        <p>
			Latar belakang pendiriannya didasari oleh keinginan yang kuat untuk mandiri, membangun kerjasama, dan dikembangkan dengan semangat serta kemauan yang kuat untuk memberikan pelayanan yang terbaik kepada pelanggan.
		</p>
        <p>
			Berbekal pengetahuan, keterampilan, dan pengalaman yang ada. Kami bertekad selalu memberikan pelayanan sepenuhnya untuk kepentingan perkembangan dan pertumbuhan usaha pelanggan.
		</p>
		<p>Kualitas dan kepuasan pelanggan menjadi prioritas kami.</p>
    </div>
    <div class="block-highlight block-pd-h block-pd-sm">
		<div class="col-md-4">
			<h3 class="block-title">
				Visi
			</h3>
			<p class="text-fancy">Menjadi perusahaan kelas dunia</p>
		</div>
		<div class="col-md-4">
			<h3 class="block-title">
				Misi
			</h3>
			<p class="text-fancy">Menjual barang yang berkualitas, berguna, dan bermutu</p>
		</div>
		<div class="col-md-4">
			<h3 class="block-title">
				Komitmen
			</h3>
			<p class="text-fancy">Memberikan pelayanan yang baik dan pengiriman yang tepat waktu</p>
		</div>
    </div>
    
@endsection