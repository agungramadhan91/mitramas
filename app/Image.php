<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Business;

class Image extends Model
{
    protected $fillable = ['umum'];

    public function business()
    {
        return $this->belongsTo('App\Business');
    }
}
