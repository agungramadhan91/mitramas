<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('business_id')->unsigned();

            $table->string('umum')->default('');
            $table->string('depan')->default('');
            $table->string('belakang')->default('');
            $table->string('kanan')->default('');
            $table->string('kiri')->default('');
            $table->string('atas')->default('');
            $table->string('bawah')->default('');

            $table->foreign('business_id')->references('id')->on('businesses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
